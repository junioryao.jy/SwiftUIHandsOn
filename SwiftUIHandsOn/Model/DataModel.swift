//
//  DataModel.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 03/12/2023.
//

import Foundation

struct Card: Identifiable {
    var name: String
    var id: UUID
    var isVisible: Bool
}
