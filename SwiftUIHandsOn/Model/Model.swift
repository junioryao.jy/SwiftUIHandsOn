//
//  Model.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 29/11/2023.
//

import Foundation
import OSLog

let log = Logger(subsystem: "-", category: "CardBusinessModel")



struct CardBusinessModel {
    var allEmojis = ["👰🏿", "👱", "👱🏻", "👱🏼", "👱🏽", "👱🏾", "👱🏿", "👲", "👲🏻", "👲🏼", "👲🏽", "👲🏾", "👲🏿", "👳", "👳🏻", "👳🏼", "👳🏽", "👳🏾", "👳🏿", "👴", "👴🏻", "👴🏼", "👴🏽", "👴🏾", "👴🏿", "👵", "👵🏻", "👵🏼", "👵🏽", "👵🏾", "👵🏿", "👮", "👮🏻", "👮🏼", "👮🏽", "👮🏾", "👮🏿", "👷", "👷🏻", "👷🏼", "👷🏽", "👷🏾", "👷🏿", "👸", "👸🏻", "👸🏼", "👸🏽", "👸🏾", "👸🏿", "💂", "💂🏻", "💂🏼", "💂🏽", "💂🏾", "💂🏿", "👼", "👼🏻", "👼🏼", "👼🏽", "👼🏾", "👼🏿", "🎅", "🎅🏻", "🎅🏼", "🎅🏽", "🎅🏾", "🎅🏿", "👻", "👹", "👺", "💩", "💀", "👽", "👾", "🙇", "🙇🏻", "🙇🏼", "🙇🏽", "🙇🏾", "🙇🏿", "💁", "💁🏻", "💁🏼", "💁🏽", "💁🏾", "💁🏿", "🙅", "🙅🏻", "🙅🏼", "🙅🏽", "🙅🏾", "🙅🏿", "🙆", "🙆🏻", "🙆🏼", "🙆🏽"]
    
    private(set) var displayedCard = [Card]()
    private var operation = CardOperation()
    
    init(numberOfEmojies: Int) {
        allEmojis.shuffle()
        for i in 0 ..< numberOfEmojies {
            buildCard(i)
        }
    }
    
    mutating func toggleCardState(card: Card) {
        if operation.moreThanTwoCardFacedUpShouldBeFacedDown(listOfReferenceCard: displayedCard) {
            displayedCard.indices.forEach { index in
                if displayedCard[index].isVisible {
                    displayedCard[index].isVisible = false
                }
            }
        }
        
        guard let cardToUpdate = operation.swithState(card: card, listOfReferenceCard: displayedCard) else {
            return
        }
        
        displayedCard[cardToUpdate.cardIndex] = cardToUpdate.card
        
        if let indexesOfIdenticalCard = operation.isPairOfCardFound(listOfReferenceCard: displayedCard) {
            let firstCardToRemove = displayedCard[indexesOfIdenticalCard[0]]
            let secondCardToRemove = displayedCard[indexesOfIdenticalCard[1]]
            displayedCard.removeAll { card in
                card.id == firstCardToRemove.id || card.id == secondCardToRemove.id
            }
        }
    }
    
    mutating func addCardOntheView() {
        buildCard(displayedCard.count)
    }
    
    mutating func removeCardOntheView() {
        _ = displayedCard.popLast()
        _ = displayedCard.popLast()
    }
    
    private mutating func buildCard(_ i: Int) {
        displayedCard.append(Card(name: allEmojis[i], id: UUID(), isVisible: false))
        displayedCard.append(Card(name: allEmojis[i], id: UUID(), isVisible: false))
    }
}


