import Foundation

struct CardOperation {
    func swithState(card: Card, listOfReferenceCard: [Card]) -> (cardIndex: Int, card: Card)? {
        let index = listOfReferenceCard.firstIndex { collectionCard in collectionCard.id == card.id }
        if let foundIndex = index {
            var cardValue = listOfReferenceCard[foundIndex]
            cardValue.isVisible.toggle()
            return (foundIndex, cardValue)
        } else {
            log.info("Bug : intruder in the system")
            return nil
        }
    }

    func isPairOfCardFound(listOfReferenceCard: [Card]) -> [Int]? {
        var indexOfVisibleCard = [Int]()
        indexOfVisibleCard = listOfReferenceCard.indices.filter { listOfReferenceCard[$0].isVisible }
        let cardIsIdentical: Bool = indexOfVisibleCard.count == 2 && listOfReferenceCard[indexOfVisibleCard[0]].name == listOfReferenceCard[indexOfVisibleCard[1]].name
        if cardIsIdentical {
            return indexOfVisibleCard
        }
        return nil
    }

    func moreThanTwoCardFacedUpShouldBeFacedDown(listOfReferenceCard: [Card]) -> Bool {
        listOfReferenceCard.filter { card in
            card.isVisible
        }.count >= 2
    }
}
