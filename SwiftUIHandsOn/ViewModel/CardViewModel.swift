//
//  CarViewModel.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 29/11/2023.
//

import SwiftUI

class CardViewModel: ObservableObject {
    @Published private var businessCard: CardBusinessModel
    
    // MARK: - VIEW MODIFIER
    
    init(numberOfCard: Int) {
        self.businessCard = CardBusinessModel(numberOfEmojies: numberOfCard)
    }
    
    func getCards() -> [Card] {
        businessCard.displayedCard
    }
    
    // MARK: INTENTS
    
    func toggle(card: Card) {
        if !card.isVisible {
            businessCard.toggleCardState(card: card)
        }
    }
    
    func increaseNumberOfCardOnTheScreen() {
        businessCard.addCardOntheView()
    }
    
    func decreaseNumberOfCardOnTheScreen() {
        businessCard.removeCardOntheView()
    }
}
