//
//  SwiftUIHandsOnApp.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 24/11/2023.
//

import SwiftUI
@main
struct SwiftUIHandsOnApp: App {
    var body: some Scene {
        WindowGroup {
            ListSwiftUIView()
//            SheetPresentationDisplayNavigationStack()
//            ContentView(cards: CardViewModel(numberOfCard: 2))
        }
    }
}
