//
//  ListSwiftUIView.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 15/12/2023.
//

import SwiftUI

struct ListSwiftUIView: View {
    @State var animalElements: [String] = ["dog", "cat", "buffalo", "chicken", "fish"]
    @State var fruitElements: [String] = ["banana", "mango", "ananas", "beans"]
    var body: some View {
        NavigationStack {
            VStack {
                topListElement(fruits: $fruitElements, animal: $animalElements)
                Spacer()
                AddingElementview(fruits: $fruitElements, animal: $animalElements)
            }.navigationTitle("Shortlist")
                .toolbar(content: {
                    ToolbarItem(placement: .topBarLeading) {
                        EditButton()
                    }
                })
        }
    }
}

struct topListElement: View {
    @Binding var fruits: [String]
    @Binding var animal: [String]
    var body: some View {
        List {
            SectionView(sectionType: .Animal, elements: $animal, color: .red)
            SectionView(sectionType: .Fruit, elements: $fruits, color: .orange)
        }.listStyle(.sidebar)
    }
}

struct AddingElementview: View {
    @State var firstItemSelected: CustomSection = .Animal
    @State var userInput: String = ""
    @Binding var fruits: [String]
    @Binding var animal: [String]
    @Environment(\.colorScheme) var screenModeColor

    var body: some View {
        VStack {
            HStack {
                Text("List section")
                    .multilineTextAlignment(.center)
                Spacer()
                Picker("Section selection", selection: $firstItemSelected) {
                    ForEach(CustomSection.allCases, id: \.self) { data in
                        Text(data.rawValue)
                    }
                }.pickerStyle(.automatic).multilineTextAlignment(/*@START_MENU_TOKEN@*/ .center/*@END_MENU_TOKEN@*/)
            }.padding(.horizontal, 26)

            TextField(text: $userInput, prompt: Text("Enter an article")) {}
                .padding(.horizontal, 32)
                .autocorrectionDisabled()

            Button(action: {
                print(userInput)
                if !userInput.isEmpty, !userInput.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    switch firstItemSelected {
                    case .Fruit: fruits.append(userInput.trimmingCharacters(in: .whitespacesAndNewlines))
                    case .Animal: animal.append(userInput.trimmingCharacters(in: .whitespacesAndNewlines))
                    }
                    userInput = ""
                }
            }, label: {
                Text("Add")
                    .padding(.horizontal, 45)
                    .padding(.vertical, 15)
                    .background(Color.accentColor)
                    .clipShape(RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/))
                    .foregroundStyle(.white)

            }).padding(.bottom)
        }.background(screenModeColor == .dark ? .black : .white)
            .clipShape(RoundedRectangle(cornerRadius: 20))
            .ignoresSafeArea(.all)
    }
}

struct MyTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(30)
            .background(
                RoundedRectangle(cornerRadius: 20, style: .continuous)
                    .stroke(Color.accentColor, lineWidth: 3)
            ).padding()
    }
}

struct SectionView: View {
    var sectionType: CustomSection
    @Binding var elements: [String]
    var color: Color

    var body: some View {
        Section(content: {
                    ForEach(elements, id: \.self) { element in
                        Text(element)
                    }.onDelete(perform: { indexSet in
                        elements.remove(atOffsets: indexSet)
                    })
                    .onMove(perform: { indices, newOffset in
                        elements.move(fromOffsets: indices, toOffset: newOffset)
                    })
                },
                header: {
                    HStack {
                        Text(sectionType.rawValue)
                            .foregroundStyle(color)
                            .font(.callout)
                        Image(systemName: "hurricane.circle")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 24, height: 24)
                            .foregroundStyle(color)
                    }
                }).listStyle(.grouped)
    }
}

enum CustomSection: String, CaseIterable {
    case Fruit, Animal
}

#Preview {
    ListSwiftUIView().preferredColorScheme(/*@START_MENU_TOKEN@*/ .dark/*@END_MENU_TOKEN@*/)
}

#Preview {
    ListSwiftUIView().preferredColorScheme(.light)
}
