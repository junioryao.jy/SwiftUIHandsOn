//
//  PopOver.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 13/12/2023.
//

import SwiftUI
// presentation detent

enum ScreenColor: Hashable {
    case bleu, brown
}

struct SheetPresentationDisplayNavigationStack: View {
    @State var popOver = false

    var body: some View {
        NavigationStack {
            ScrollView {
                Text("Hello world MG ")
                Text("Hello world MG ")
                Text("Hello world MG ")
                NavigationLink("bleuScreen", value: ScreenColor.brown)
            }.navigationDestination(for: ScreenColor.self) { color in
                switch color {
                case .bleu: OrangeView()
                case .brown: BrownView()
                }
            }
            .sheet(isPresented: $popOver, content: { // pop over with sheet
                PopOverSubView(close: $popOver)
            })
            .navigationTitle("navigation Title")
            .toolbar {
                ToolbarItem(placement: .topBarLeading) { // we got leading , trailing as well , pricipal is to center elements
                    Button(action: {
                        popOver.toggle()
                    }, label: {
                        Image(systemName: "gear")
                    }).foregroundStyle(.red)
                }
            }
        }
    }
//        .fullScreenCover(isPresented: $popOver, content: {
//            PopOverSubView() // need environnement to customize the exit button
//        })
//        .sheet(isPresented: $popOver, content: {
//            PopOverSubView()
//        })
}

struct PopOverSubView: View {
    @Binding var close: Bool
    var body: some View {
        ZStack(alignment: .topLeading) {
            Color.yellow.ignoresSafeArea(.all)
            Image(systemName: "xmark.square")
                .onTapGesture {
                    close.toggle()
                }
                .font(.largeTitle)
                .padding(.all, 20)
        }
    }
}

struct OrangeView: View {
    var body: some View {
        Color.orange.ignoresSafeArea()
    }
}

struct BrownView: View {
    var body: some View {
        ZStack {
            Color.brown.ignoresSafeArea()
            NavigationLink("bleuScreenKindaOrange", value: ScreenColor.bleu)
        }.navigationTitle("BrownScreen")
    }
}
// can use pop over with subview as well as pop over with transition
// advantage of transition we can customize our view , our transition
struct initPopOver: View {
    @State var popOver = false
    var body: some View {
        VStack {
            Button(/*@START_MENU_TOKEN@*/"Button"/*@END_MENU_TOKEN@*/) {
                popOver.toggle()
            }
            Spacer()
            if popOver {
                PopOverSubView(close: $popOver)
                    .transition(.move(edge: .bottom)) // pop over with transition , of our custom content
                    .animation(.easeInOut)
            }
        }
    }
}

#Preview {
    SheetPresentationDisplayNavigationStack()
//    PopOverSubView()
}
