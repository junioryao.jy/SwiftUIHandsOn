//
//  Color.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 03/12/2023.
//
//shape 

import SwiftUI

struct ColorTest: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 30)
            .frame(width: 300, height: 300)
            .foregroundColor(/*@START_MENU_TOKEN@*/ .yellow/*@END_MENU_TOKEN@*/)
            .shadow(color: /*@START_MENU_TOKEN@*/.black/*@END_MENU_TOKEN@*/.opacity(0.3 ), radius: 10, x: -15, y: -10)
    }
}

#Preview {
    ColorTest()
}
