//
//  Map.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 03/12/2023.
//

import SwiftUI

struct Map: View {
    var body: some View {
        VStack {
            RoundedRectangle(cornerRadius: 40, style: RoundedCornerStyle.circular)
                .frame(width: 200, height: 100)
//            Capsule(style: .circular)
//            Ellipse().frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height:50)
//            Circle()
//                .fill(.yellow)
//                .foregroundColor(Color("customeColor"))
//                .stroke(/*@START_MENU_TOKEN@*/Color.blue/*@END_MENU_TOKEN@*/) // blue outline
//                .trim(from:0.3,to:1)
//                .stroke(/*@START_MENU_TOKEN@*/Color.blue/*@END_MENU_TOKEN@*/, lineWidth: 23)
//                .stroke(Color.purple, style: StrokeStyle(lineWidth: 18, lineCap: CGLineCap.round, dash: [12,35]))
        }
    }
}

#Preview {
    Map()
}
