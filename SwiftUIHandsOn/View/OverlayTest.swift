//
//  SwiftUIView.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 05/12/2023.
//

import SwiftUI

struct OverlayTest: View {
    var body: some View {
        Text("DAMN GOOD").font(.title2).bold()
            .background(
                Circle().fill(
                    LinearGradient(colors: [Color("customColor"), Color.cyan, .green], startPoint: .bottomLeading, endPoint: .topTrailing)
                )
                .frame(width: 400, height: 400, alignment: .center)
                .overlay(content: {
                    RoundedRectangle(cornerRadius: 50)
                        .foregroundColor(.yellow)
                        .frame(width: 150, height: 150)
                        .opacity(/*@START_MENU_TOKEN@*/0.8/*@END_MENU_TOKEN@*/)
                        .shadow(color: /*@START_MENU_TOKEN@*/ .black/*@END_MENU_TOKEN@*/, radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: 10, y: -5)
                })
            )
            .foregroundColor(.red)
            .italic()
    }
}

#Preview {
    OverlayTest()
}
