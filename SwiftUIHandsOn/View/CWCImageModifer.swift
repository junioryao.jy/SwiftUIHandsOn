//
//  CWCImageModifer.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 11/12/2023.
//

import SwiftUI

struct CWCImageModifer: View {
    var body: some View {
        VStack {
            Spacer()
            CustomView(titleOne: "EIFFEL Tower", titleCity: "Paris", imageName: "paris")
            Spacer()
            CustomView(titleOne: "Liberty Status", titleCity: "London", imageName: "ny")
            Spacer()
        }
    }
}

struct CustomView: View {
    var titleOne: String
    var titleCity: String
    var imageName: String
    var body: some View {
        ZStack {
            Image(imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .clipShape(RoundedRectangle(cornerRadius: 15))
                .padding(.all, 20)
            VStack {
                Text(titleOne).font(.title)
                Text(titleCity).font(.footnote)

            }.foregroundColor(.white)
                .padding(15)
                .background(.black.opacity(0.3))
                .clipShape(RoundedRectangle(cornerRadius: 15))
        }
    }
}

#Preview {
    CWCImageModifer()
}
