//
//  Padding.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 05/12/2023.
//

import SwiftUI

struct Padding: View {
    var body: some View {
        
        VStack {
            Spacer()
            HStack {
                Button(action: {
                    //  cards.decreaseNumberOfCardOnTheScreen()
                }, label: {
                    VStack {
                        Image(systemName: "minus.rectangle")
                        Text("Decrease").dynamicTypeSize(.xSmall)
                    }
                })
                Spacer()
                //        Slider(value: 0, in: 0.4 ... 1.5)
                Button(action: {
                    //  cards.increaseNumberOfCardOnTheScreen()
                }, label: {
                    VStack {
                        Image(systemName: "plus.rectangle")
                        Text("Increase").dynamicTypeSize(.xSmall)
                    }
                })
            }.padding()
        }
        
        
    }
}

#Preview {
    Padding()
}
