//
//  ContentView.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 24/11/2023.

import SwiftUI

struct ContentView: View {
    @ObservedObject var cards: CardViewModel
    let columns = [
        GridItem(.adaptive(minimum: 80)),
    ]
    @State var aspectRation = 2 / 2.5
    var body: some View {
        VStack {
            ScrollView {
                LazyVGrid(columns: columns) {
                    ForEach(cards.getCards()) { emoji in
                        cardView(emoji: emoji)
                            .aspectRatio(aspectRation, contentMode: .fill)
                            .onTapGesture {
                                withAnimation(Animation.spring) {
                                    cards.toggle(card: emoji)
                                }
                            }
                    }
                }
            }
            HStack {
                buttonControl(cards: cards, aspectRation: $aspectRation)
            }
        }.padding(.horizontal)
    }
}

struct cardView: View {
    var emoji: Card
    var body: some View {
        ZStack {
            if emoji.isVisible {
                RoundedRectangle(cornerRadius: 20)
                    .foregroundColor(.blue)
                Text(emoji.name)
                    .fontWeight(.bold)
                    .font(.largeTitle)
                    .multilineTextAlignment(.center)
                    .foregroundColor(.yellow)
            } else {
                RoundedRectangle(cornerRadius: 20)
                    .foregroundColor(.green)
            }
        }
    }
}

struct buttonControl: View {
    var cards: CardViewModel
    @Binding var aspectRation: Double
    var body: some View {
        Button(action: {
            withAnimation(Animation.easeIn) {
                cards.decreaseNumberOfCardOnTheScreen()
            }
        }, label: {
            VStack {
                Image(systemName: "minus.rectangle")
                Text("Decrease").dynamicTypeSize(.xSmall)
            }
        })
        Spacer()
        Slider(value: $aspectRation, in: 0.4 ... 1.5)
        Spacer()
        Button(action: {
            withAnimation(Animation.easeOut) {
                cards.increaseNumberOfCardOnTheScreen()
            }
        }, label: {
            VStack {
                Image(systemName: "plus.rectangle")
                Text("Increase").dynamicTypeSize(.xSmall)
            }
        })
    }
}

#Preview {
    ContentView(cards: CardViewModel(numberOfCard: 2))
}
