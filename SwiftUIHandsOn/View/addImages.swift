//
//  addImages.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 04/12/2023.
//

import SwiftUI
// frame can be stuck up on any type of screen
struct addImages: View {
    var body: some View {
        VStack {
            Image(systemName: "externaldrive.fill.badge.plus")
                .resizable(resizingMode: .tile)
                .frame(width: 200, height: 200)
                .scaledToFit()
                .foregroundColor(/*@START_MENU_TOKEN@*/ .blue/*@END_MENU_TOKEN@*/)

            Image("brain")
                .resizable()
                .scaledToFit()
                .frame(width: 300, height: 300) // cornerRadius of an image , clipShape ,

            Text("Testing Frame")
                .multilineTextAlignment(.center)
                .bold()
                .foregroundColor(.red)
                .background(.blue)
                .frame(maxWidth: /*@START_MENU_TOKEN@*/ .infinity/*@END_MENU_TOKEN@*/, minHeight: 100)
                .background(.green)
                .frame( maxHeight:  .infinity)
                .background(.pink)
  
        }
    }
}

#Preview {
    addImages()
}
