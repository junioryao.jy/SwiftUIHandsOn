//
//  cwcModifier.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 11/12/2023.
//

import SwiftUI

struct cwcModifier: View {
    var body: some View {
        Text("Hello, World To everyBody")
            .padding(.all, 40)
            .background(Color.green)
            .clipShape(RoundedRectangle(cornerRadius: 25.0))
            .frame(width: 330, height: 150)
            .background(.blue)
            .clipShape(RoundedRectangle(cornerRadius: 25.0))
    }
}

#Preview {
    cwcModifier()
}
