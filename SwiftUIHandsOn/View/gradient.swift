//
//  gradient.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 03/12/2023.
//

// extension of color
 extension Color {
     static var customColor = Color("cusromColor")
 }

import SwiftUI
// linear gradient radian
// Color(#colorLiteral())

struct gradient: View {
    //declaration of color
    var customColor = Color("cusromColor")
    var randomColor = Color(#colorLiteral(red: 0.8132066131, green: 0.4939912558, blue: 0.9232118726, alpha: 1))
    var body: some View {
        RoundedRectangle(cornerRadius: 30)
            .fill(LinearGradient(gradient: Gradient(colors: [.customColor, .red, randomColor]), startPoint: .bottomTrailing, endPoint: .topLeading))
            .frame(width: 300, height: 300)
            .shadow(color: /*@START_MENU_TOKEN@*/ .black/*@END_MENU_TOKEN@*/ .opacity(0.3), radius: 10, x: -15, y: -10)
    }
}

#Preview {
    gradient()
}
