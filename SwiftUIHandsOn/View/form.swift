//
//  form.swift
//  SwiftUIHandsOn
//
//  Created by Junior Yao on 03/12/2023.
//

import SwiftUI
// shape 
struct form: View {
    @State var notifyMeAbout: String = "Direct Mesage"
    @State var playNotificationSounds = true
    @State var sendReadReceipts = true
    @State var profileImageSize = 123

    var body: some View {
        Form {
            Section(header: Text("Notifications").bold()) {
                Picker("Notify Me About", selection: $notifyMeAbout) {
                    Text("Direct Messages").tag(NotifyMeAboutType.directMessages)
                    Text("Mentions").tag(NotifyMeAboutType.mentions)
                    Text("Anything").tag(NotifyMeAboutType.anything)
                }//.selectionDisabled(false)
                Toggle("Play notification sounds", isOn: $playNotificationSounds)
                Toggle("Send read receipts", isOn: $sendReadReceipts)
            }
            Section(header: Text("User Profiles").bold()) {
                Picker("Profile Image Size", selection: $profileImageSize) {
                    Text("Large").tag(ProfileImageSize.large)
                    Text("Medium").tag(ProfileImageSize.medium)
                    Text("Small").tag(ProfileImageSize.small)
                }
                Button("Clear Image Cache") {}
            }
            .headerProminence(/*@START_MENU_TOKEN@*/.increased/*@END_MENU_TOKEN@*/)
        }
    }
}

enum NotifyMeAboutType {
    case directMessages, mentions, anything
}

enum ProfileImageSize {
    case large, medium, small
}

#Preview {
    form()
}
